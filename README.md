**DataSet selection :** 

- Dataset decided which has anonymized data from patients seen at the Hospital Israelita Albert Einstein,at São Paulo, Brazil, and who had samples collected to perform the SARS-CoV-2 RT-PCR and additional laboratory tests during a visit to the hospital.

- WHO site


**Analysis that can be done using data set :**

- Analyse the parameters responsible for admission to general ward, semi-intensive unit or intensive care unit among confirmed COVID-19 cases and visualize it using different statistics models.

 - Predict and present the result of admission to general ward, semi-intensive unit or intensive care unit among confirmed COVID-19 cases on R-shiny Dashboard.
